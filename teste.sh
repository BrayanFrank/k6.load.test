#!/bin/bash

export IDBOT=de433ad5-ad3c-4be2-b349-887ea94ce4e6
export IDBOT2=1dc8d9c3-20d9-4951-9509-870cfc193399
export IDBOT3=6e10d433-e434-483a-ab47-541824f9f8a4

k6 run -e IDBOT=$IDBOT samples/myscripts/backoffice/backoffice-training.js > /dev/null &
k6 run -e IDBOT=$IDBOT2 samples/myscripts/backoffice/backoffice-training.js > /dev/null &
k6 run -e IDBOT=$IDBOT3 samples/myscripts/backoffice/backoffice-training.js > /dev/null &

