import http from "k6/http";
import { check } from "k6";

//Mudança de valores 

export var variables = {
    "environmet" : "dev",
    "botId" : "124d7e15-3b57-463c-a4c2-afc4614b215c",
    "idBotVersion" : "29803495-6c7d-4e39-9f3a-2d9a479dd836",
    "typeName" : "Simples",
    "type" : 0,
    "auth" : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImJyYXlhbi5zb2FyZXNAY2Vkcm90ZWNoLmNvbSIsImlkIjoiNzU2NTRjMDktZmIzYy00YTE2LWFlNjQtNDg0MzRjM2RkOTI2IiwiaXNzIjoidmQ4Snk1dG1IcnFMc1B2NEo3U1BldW5ENXB4MEpZb24iLCJpYXQiOjE1ODI1NTE5ODIsImV4cCI6MTU4MjU1NTU4Mn0.8Bog-ZiZOlM4A-nXrvAGER_ol5jdajeWW6QrJTgJDOs"
}

export var options = {
    vu:1,
    iterations: 5
}

//

export default function() {

    let payload = JSON.stringify({
        "name":"Entidade." + __VU + "." + __ITER,
        "type": 0,
        "typeName": "Simples"
    })

    let headers = {
        "Content-Type" : "application/json",
        "Authorization" : variables.auth
    }

    var res = http.post("https://" + variables.environmet +".people.com.ai/backoffice/api/v4/bot/" + variables.botId + "/version/" + variables.idBotVersion + "/entity", payload, {headers : headers})

    check( res,{
        "Response Code = 201" : (res) => res.status == 201,
        "Response Code = 401" : (res) => res.status == 401,
        "Response Code = 409" : (res) => res.status == 409,
        "Response Code = 500" : (res) => res.status == 500
    })
}