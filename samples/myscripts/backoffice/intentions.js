import http from "k6/http";
import { check } from "k6";

export var variables = {
    "environment" : "dev",
    "idCategory" : "639b0b6e-4119-45dc-84e9-85304072cdd7",
    "auth" : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImJyYXlhbi5zb2FyZXNAY2Vkcm90ZWNoLmNvbSIsImlkIjoiNzU2NTRjMDktZmIzYy00YTE2LWFlNjQtNDg0MzRjM2RkOTI2IiwiaXNzIjoiWVQ5Q1dBYjRtbVJFa254OU5DME9tMTF4N1pkSzg3Q1YiLCJpYXQiOjE1ODIyMjc0NTcsImV4cCI6MTU4MjIzMTA1N30.7cuqPqCe4YH_lIDEDGLrbrVRholJ840WiNkt-XxA0Js"
};

export var options = {
    vus: 1,
    iterations: 1
};

export default function() {

    let payload = JSON.stringify({
        "idCategory" : variables.idCategory,
        "name" : "Idfdfdfnção" + __VU + "" + __ITER,
        "disambiguationEnabled" : true
    });

    let headers = {
        "Authorization" : variables.auth,
        "content-type" : "application/json;charset=UTF-8"
    }

    var res = http.post("https://" + variables.environment + ".people.com.ai/backoffice/api/v4/intent", payload, { headers : headers });

    check( res,{
        "Response Code = 201" : (res) => res.status == 201,
        "Response Code = 401" : (res) => res.status == 401,
		"Response Code = 500" : (res) => res.status == 500
    })
}