import http from "k6/http";
import { check } from "k6";

// Mudanças de valores

export var variables = {
    "expressionName" : "expressao",
    "auth" : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImJyYXlhbi5zb2FyZXNAY2Vkcm90ZWNoLmNvbSIsImlkIjoiNzU2NTRjMDktZmIzYy00YTE2LWFlNjQtNDg0MzRjM2RkOTI2IiwiaXNzIjoidmQ4Snk1dG1IcnFMc1B2NEo3U1BldW5ENXB4MEpZb24iLCJpYXQiOjE1ODI1NTE5ODIsImV4cCI6MTU4MjU1NTU4Mn0.8Bog-ZiZOlM4A-nXrvAGER_ol5jdajeWW6QrJTgJDOs",
    "environment" : "dev"
}

export var options = {
	vus: 1,
	iterations: 1
}

//

export default function(){
    let payload = JSON.stringify({
            "exampleText": variables.expressionName + ":" + __VU + "." + __ITER ,
            "idIntent": __ENV.IDINTENT, // É necessário passar -e IDINTENT=valor como CLI flag
            "entity":[],
            "entityLabels":[]
        })

    let headers = {
        "Authorization" : variables.auth,
        "content-type" : "application/json;charset=UTF-8"
    }

    var res = http.post("https://" + variables.environment + ".people.com.ai/backoffice/api/v4/expression", payload, {headers : headers});
    
    check( res,{
        "Response Code = 201" : (res) => res.status == 201,
        "Response Code = 401" : (res) => res.status == 401,
		"Response Code = 500" : (res) => res.status == 500
    })
}