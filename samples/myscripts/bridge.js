import http from "k6/http";
import { check } from "k6";

var responseURL = "https://apidev.people.com.ai/api/channel/generic/124d7e15-3b57-463c-a4c2-afc4614b215c";

export var variables = {

	"token" : "9529bed8-b32f-4fc2-ac02-ac92841b24bf"
};

export let options = {

	vus: 10,
	duration: "60s"
};


export default function() {

  let headers = { "X-Auth-Token" : variables.token };
  
  let params = JSON.stringify({
		"channelId": "string",
		"channelAlias": "string",
		"conversationId": "conversation:" + __VU + "." + __ITER,
		"conversationReference": "reference:" + __VU + "." + __ITER,
		"userId": "user:" + __VU + "." + __ITER,
		"userName": "username:" + __VU + "." + __ITER,
		"message": "Mensagem:" + __VU,
		"messageType": "message" 
  });

  let res = http.post(responseURL, params, {headers: headers});

  check(res, {
    "Response Code = 200" : (res) => res.status == 200
  });

}
