import http from "k6/http";
import { check } from "k6";

//Change values here

export var variables = {
	"name" : "Recurso ",
	"idBot" : "124d7e15-3b57-463c-a4c2-afc4614b215c",
	"idCompany" : "d8b238a3-f423-4956-83a8-d3edae7b18b0",
	"idCategory" : "3d2a989e-cbd7-477a-a685-e649e8477298",
	"auth" : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImJyYXlhbi5zb2FyZXNAY2Vkcm90ZWNoLmNvbSIsImlkIjoiNzU2NTRjMDktZmIzYy00YTE2LWFlNjQtNDg0MzRjM2RkOTI2IiwiaXNzIjoidmQ4Snk1dG1IcnFMc1B2NEo3U1BldW5ENXB4MEpZb24iLCJpYXQiOjE1ODI1NTE5ODIsImV4cCI6MTU4MjU1NTU4Mn0.8Bog-ZiZOlM4A-nXrvAGER_ol5jdajeWW6QrJTgJDOs",
    "environment" : "dev"
}

export var options = {
	vus: 3,
	duration: "10s"
}

//

export default function(){
	let payload = JSON.stringify({
		"nodes":[],
		"edges":[],
		"exit":{},
		"name": variables.name + ":" + __VU + "." + __ITER,
		"idBot": variables.idBot,
		"idCompany": variables.idCompany,
		"idCategory": variables.idCategory,
		"idIntent":null,
		"interactive":null,
		"nodesCount":0,
		"dialogVariables":[]
	})

	let headers = {
		"Authorization" : variables.auth
	}

	var res = http.post("https://" + variables.environment + ".people.com.ai/resource/api/resource", payload, {headers : headers});

	check( res,{
		"Response Code = 201" : (res) => res.status == 201,
		"Response Code = 401" : (res) => res.status == 401,
		"Response Code = 500" : (res) => res.status == 500
	})
}


