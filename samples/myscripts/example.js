import http from "k6/http";

export var variables = {
  "url" : "http://5e2737c66eeb4400145369a0.mockapi.io/api",
}

export let options = {
  vus: 1,
	iterations: 1
};

export default function() {

  let res = http.get(variables.url + "/get");

  let content = { "name" : res.body}
  
  let rep = http.post(variables.url + "/post", content);

}

